8.x-1.0 - 2017-12-17
--------------------------
- No issues so far, closing official 8.x-1.0.

8.x-1.0-beta2 - 2017-10-02
--------------------------
- Merged the two blacklists (standard and regular expression items) to a single list.
- Blacklist can now be exported/imported to/from a JSON file.
- Blacklist JSON import can be scheduled and executed automatically with cron.
- Subdomains won't be matched by standard blacklist items anymore, use regular expressions instead.

8.x-1.0-beta1 - 2017-08-13
--------------------------
- First public release.
